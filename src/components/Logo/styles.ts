import styled from 'styled-components/native';


export const Image = styled.Image`
    width: 100%;
    height: 134px;
    margin-left: 24px;
    margin-top: 48px;
`