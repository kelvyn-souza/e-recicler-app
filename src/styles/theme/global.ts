export default {
    pallete: {
        colors:{
            primary: '#4CAF50',
            secondary: '#607D8B',
            text: '#FFFFFF',
        }
    },

    fonts: {
        robotoRegular: 'Roboto_400Regular', 
        robotoBold: 'Roboto_700Bold', 
        ubuntuRegular: 'Ubuntu_400Regular', 
        ubuntuBold: 'Ubuntu_700Bold'
    }
}